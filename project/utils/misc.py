def make_mapping(iterable:list, el_str:str="elem")->dict:
    '''
    Arguments:
        iterable (list): a list like item where each element in the list should
            be mapped into the range (0, len(iterable))
        el_str (str): a programmer friendly str to name the mapping of the
            elements of iterbale to their index. Defaults to "elem"

    Retuns:
        map (dict): a dictionary with two keys: subdictionary pairs:
            - "iloc" : returns the dictionary whereby an index retrieves an element
            - el_str : returns the dictionary whereby an element retrieves its index
    '''
    imap = { "iloc": {}, el_str: {}}
    for i, e in enumerate(iterable):
        imap["iloc"][i] = e
        imap[el_str][e] = i
    return imap
