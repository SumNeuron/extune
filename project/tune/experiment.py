import ray, os, sys
from ray import tune

from project.sacred.experiment import  ex

# wrapper function over sacred experiment
def trainable(config, reporter):
    '''
    Arguments:
        config (dict): Parameters provided from the search algorithm
            or variant generation.
        reporter (Reporter): Handle to report intermediate metrics to Tune.
    '''
    ex.run(config_updates=config)
    result = ex.current_run.result
    reporter(result=result, done=True)


ray.init(ignore_reinit_error=True)
def run_fn(ex_config:dict) -> None:
    '''
    Arguments:
        ex_config (dict): the tune experiment configuration dictionary with all
        values to be specfied except `'run'`, which is set to be the traininable
        wrapper over the sacred experiment.
    '''
    tune.register_trainable('trainable', trainable)
    tune.run_experiments({
        'demo': {
            **ex_config,
            'run':'trainable'
        }
    })
