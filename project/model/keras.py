from keras.layers import Input, Embedding, Dot, Reshape, Dense
from keras.models import Model
import random, numpy as np, pandas as pd
from keras.callbacks import Callback




def twin_embedding_model(
    dims_twin_a: int,
    dims_twin_b: int,
    dims_latent: int = 50,
    name_twin_a: str = 'twin_a',
    name_twin_b: str = 'twin_b',
    classification: bool = False
):
    '''
    Arguments:
        dims_twin_a (int): dimensions of twin a.

        dims_twin_b (int): dimensions of twin b.

        dims_latent (int): dimensions of the embedding.

        name_twin_a (str): name of the input, and uses in naming the
            corresponding embedding layer. Defaults to 'twin_a'

        name_twin_b (str): name of the input, and uses in naming the
            corresponding embedding layer. Defaults to 'twin_b'

        classification (bool): whether or not to train with the objective of
            classification or regression
    Returns:
        model (keras.Model): a model for embedding concurrent values together.
    '''
    # name the inputs
    twin_a = Input(name = name_twin_a, shape = [1])
    twin_b = Input(name = name_twin_b, shape = [1])

    # Embedding twin_a (shape will be (None, 1, 50))
    twin_a_emb = Embedding(
        name = '{name_twin_a}_embedding'.format(name_twin_a=name_twin_a),
        input_dim  = dims_twin_a,
        output_dim = dims_latent
    )(twin_a)

    # Embedding twin_b (shape will be (None, 1, 50))
    twin_b_emb = Embedding(
        name = '{name_twin_b}_embedding'.format(name_twin_b=name_twin_b),
        input_dim  = dims_twin_b,
        output_dim = dims_latent
    )(twin_b)

    # Merge the layers with a dot product along the second axis (shape will be (None, 1, 1))
    merged = Dot(name = 'dot_product', normalize = True, axes = 2)([twin_a_emb, twin_b_emb])

    # Reshape to be a single number (shape will be (None, 1))
    merged = Reshape(target_shape = [1])(merged)

    if classification:
        # Squash outputs for classification
        merged = Dense(1, activation = 'sigmoid')(merged)
        model  = Model(inputs = [twin_a, twin_b], outputs = merged)
        model.compile(optimizer = 'Adam', loss = 'binary_crossentropy',  metrics = ['accuracy'])
    else:
        model = Model(inputs = [twin_a, twin_b], outputs = merged)
        model.compile(optimizer = 'Adam', loss = 'mse', metrics=['mse', 'mae', 'mape', 'cosine'])
    return model


def generate_batched_twins(
    paired_twins: list,
    map_twin_a: dict,
    map_twin_b: dict,
    positive_examples: int = 50,
    negative_ratio: float = 1.0,
    negative_label: float = -1,
    name_twin_a: str = 'twin_a',
    name_twin_b: str = 'twin_b',
):
    '''
    Arguments:
        paired_twins (list): a list of tuples (twin_a_value, twin_b_value) from
            which to produce the generating function

        map_twin_a (dict): a mapping from (`utils.misc.make_mapping`) indicating
            which index corresponds to the elements of twin_a and vis versa

        map_twin_b (dict): a mapping from (`utils.misc.make_mapping`) indicating
            which index corresponds to the elements of twin_b and vis versa

        positive_examples (int): number of elements from `paired_twins` to
            include in a batch.

        negative_ratio (float): the relative amount of negative examples
            (elements not found in `paired_twins`) to include in each batch.

        negative_label (float): the value to be associated with negative
            examples (e.g. 0 for classification -1 for regression)

        name_twin_a (str): the name of the input for the `twin_embedding_model`.
            By default 'twin_a'

        name_twin_b (str): the name of the input for the `twin_embedding_model`.
            By default 'twin_b'
    Returns:
        yielded (tuple): the next training batch as a tuple (features, labels) 
    '''
    # Create empty array to hold batch
    batch_size = int(positive_examples * (1 + negative_ratio))
    batch = np.zeros((batch_size, 3))

    range_twin_a = list(range(len(map_twin_a['iloc'])))
    range_twin_b = list(range(len(map_twin_b['iloc'])))

    paired_twins_set = set([tuple(pair) for pair in paired_twins])

    # Continue to yield samples
    while True:

        # Randomly choose positive examples
        for i, (twin_a, twin_b) in enumerate(random.sample(paired_twins, positive_examples)):
            batch[i, :] = (map_twin_a['elem'][twin_a], map_twin_b['elem'][twin_b], 1)
        i += 1

        # Add negative examples until reach batch size
        while i < batch_size:

            # Random selection
            random_twin_a = random.choice(range_twin_a)
            random_twin_b = random.choice(range_twin_b)

            # Check to make sure this is not a positive example
            if (random_twin_a, random_twin_b) not in paired_twins_set:

                # Add to batch and increment index
                batch[i, :] = (random_twin_a, random_twin_b, negative_label)
                i += 1

        # Make sure to shuffle order
        np.random.shuffle(batch)
        yield {name_twin_a: batch[:, 0], name_twin_b: batch[:, 1]}, batch[:, 2]
