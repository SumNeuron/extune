# `project.model`
The purpose of the `project.model` submodule is to decouple, as much as possible,
the code necessary for making, training, and evaluating the model from the code
needed to convert it to `Sacred` experiment and the code needed to do
hyper-parameter optimization via `Tune`.

Three "high" level functions are imported into the `__init__.py` file:

- `model_fn`: the function for making the model
- `train_fn`: the function for training the model
- `input_fn`: the function for preparing the data to be feed into the model
