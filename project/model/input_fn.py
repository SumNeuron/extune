from .keras import generate_batched_twins

def input_fn(data, config,  _run=None, _log=None):
    '''
    Arguments:
        data: data from which to produce the generator function. Here we assume
            that data is a tuple containing:
            - paired_twins (list): a list of tuples containing the paired values
            - map_twin_a (dict): mapping between elements and indicies
            - map_twin_b (dict): mapping between elements and indicies
            both of the above maps are made from `utils.misc.make_mapping`

        config (dict): configuration options.
        _run (sacred run): the _run object from a sacred experiment. By default
            None.
        _log (sacred log): the _log object from a sacred experiment. By default
            None.
    '''
    (twins, map_twin_a, map_twin_b) = data
    fn = generate_batched_twins(
        paired_twins      = twins,
        map_twin_a        = map_twin_a,
        map_twin_b        = map_twin_b,
        positive_examples = config["positive_examples"],
        negative_ratio    = config["negative_ratio"],
        negative_label    = config["negative_label"],
        name_twin_a       = config["name_twin_a"],
        name_twin_b       = config["name_twin_b"]
    )
    return fn
