from project.tune.experiment import run_fn
from ray import tune
import argparse
parser = argparse.ArgumentParser(description="ExTune Example")
parser.add_argument('--epochs', type=int, default=1, help='number of epochs to run for.')

arguments = vars(parser.parse_known_args()[0])


tune_ex_config = {
    'stop': { 'result': 0.1 },
    'config': {
        'latent_factors':  tune.grid_search([1, 2, 3]),
        'epochs': arguments['epochs'],
    },
    'resources_per_trial': {
        'cpu': 1,
        'gpu': 1
    },
    'num_samples': 1
}


if __name__ == '__main__':
    run_fn(tune_ex_config)
